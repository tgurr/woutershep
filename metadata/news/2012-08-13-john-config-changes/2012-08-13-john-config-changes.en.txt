Title: john config changes
Author: Wouter van Kesteren <woutershep@gmail.com>
Content-Type: text/plain
Posted: 2012-08-13
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: app-security/john[<1.7.9_p6]

Since 1.7.9_p6 upstream changed the config files.

Upstream's default config is now /usr/share/john/john.conf,
this has an include to /etc/john.conf for local system settings.

Please make sure there is nothing but your own custom additions in
/etc/john.conf or john might not work properly. If you have not modified the defaults
it is safe to `rm /etc/john.conf` before upgrading.
